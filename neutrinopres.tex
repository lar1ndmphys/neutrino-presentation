\documentclass{beamer}
\usetheme{Goettingen}
\usecolortheme{default}
\usepackage{epstopdf}
\usepackage{caption}
%\renewcommand{\figurename}{}
\setbeamertemplate{caption}{\insertcaption} \setbeamertemplate{caption label separator}{}


\title {Initial Sensitivity Evaluation of the LAr1ND and MicroBooNE Detectors}
\author {Augustus Porter and Colton Hill}
\institute
	{\inst{1}%
	University of Manchester
	\and
	\inst{2}%
	FermiLab
	}
\date{}

\begin{document}
%%\begin{frame}
%%\end{frame}
\frame{\titlepage}



\begin{frame}
	\frametitle{Experimental Background}
	\begin{columns}
	\column{0.5\textwidth}
		\begin{itemize}
		\item Neutrinos are most detectable through charged current (CC) or neutral current (NC) interactions.
		\item Measuring NC allows measurement of total neutrino flux.
		\\
		\item This is useful because neutrino flavours oscillate.
		\end{itemize}

	\column{0.5\textwidth}
		\begin{figure}
			\includegraphics[scale=0.55]{nc.jpg}
		\end{figure}
			\begin{itemize} 
			\item Neutral current interaction. 
			\item X is an active neutrino flavour.
			\end{itemize}
	\end{columns}
\end{frame}




\begin{frame}
	\frametitle{Sterile Neutrinos}
	Sterile neutrinos are neutrinos which do not interact via the weak interaction.\\
	\begin{itemize}
		\item From LSND - excess of \(\bar{\nu}_{\mu}\rightarrow\bar{\nu}_{e}\) events observed at mass splitting $\Delta m^{2}\approx 1eV^{2}$\\
		\vspace{2.5mm}
		\item Consistent with presence of sterile neutrinos:\\
		$\Rightarrow$  \(\bar{\nu}_{\mu}\rightarrow \nu_{sterile} \rightarrow\bar{\nu}_{e}\)
		\item From MiniBooNE - inconclusive
	\end{itemize}
	Shortcomings
	\begin{itemize}
	\item LSND: low statistics, high background
	\item MiniBooNE: inability to distinguish $e^{\pm}$ and $\gamma$
	\end{itemize}

\end{frame}




\begin{frame}
	\frametitle {Neutrino Oscillations and Sterile Flavours}
	\begin{itemize}
	\item Neutrino oscillation formula for sterile flavours:\\
	\begin{centering}{
		\begin{equation}
			\resizebox{.8\hsize}{!}
			{$\text{P}(\nu_ x \rightarrow \nu_s)=\text{sin}^2(2\theta)\text{sin}^2\left(\frac{1.27\Delta m^2 (eV^2) L (km)}{E(GeV)}\right)$}
		\end{equation} 
		}
	\end{centering}
	\item x are active flavours ($e$, $\mu$, $\tau$), $s$ is a sterile flavour state
	\vspace{2mm}
	\item \(sin^{2}(2\theta)\) is oscillation amplitude\\
		\vspace{2.5mm}
		\hspace{1.5mm} Can be defined as:
		\vspace{2.5mm}
		\begin{itemize}
		\item \(sin^{2}(2\theta_{(x/s)s})=4|U_{(x/s)4}|^{2}(1-|U_{(x/s)4}|^{2})\)
		\vspace{2.5mm}
		\item Where \(|U_{s4}|^{2}=1-\sum\limits_{x}|U_{x4}|^{2}\) 
		\item And \(|U_{\tau 4}|^{2}=0\) for simplicity.
		\end{itemize}
	\end{itemize}
\end{frame}



\begin{frame}
	\frametitle {LAr1ND and MicroBooNE Detectors}
	Located at the Booster Neutrino Beam at FermiLab
	\begin{itemize}
		\item LAr1ND is the near detector (100m)
		\item MicroBooNE is the far detector (470m)
	\end{itemize}
	The general advantages of the new detector system over LSND and MiniBooNE:
	\begin{itemize}
		\item Consider NC disappearance events with higher statistics
		\item Instead of Cherenkov detectors, use liquid Argon, differentiating between $e^{\pm}$ and $\gamma$
	\end{itemize}
	These suppress background and prevent particle misID
\end{frame}



\begin{frame}
	\frametitle {Statistical Method}
	How sensitive is the system to a NC disappearance signal?
	\begin{itemize}
	\item Consider neutral current disappearance events ($\mu$,$e$$\rightarrow$$s$) using Monte Carlo simulation
	\item Use $\chi^{2}$ method:
	{\small \begin{equation}
\chi^2 = \sum_{i,j=ND,\mu B} \left(N_i^{null} - N_i^{osc})M_{ij}^{-1}(N_j^{null}-N_j^{osc}\right)
\end{equation}}
	\item where \begin{equation} N^{osc}=N^{osc}(U_{e4},U_{\mu 4},\Delta m_{41}^2) \end{equation}
	\item and $M_{ij}^{-1}$ is the inverse covariance matrix
	\item Note: currently the Monte Carlo does not consider energy depedence for the cross-section
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Results - Sensitivity Evaluation over several parameters}
	{\small Generated results from the unchanged Monte Carlo:}
	\begin{columns}
		\column{0.5\textwidth}
			\begin{figure}[h]
				\includegraphics[scale=0.26]{NC_sens_ms0.eps}
			\end{figure}

		\column{0.5\textwidth}
			\begin{figure}[h]
				\includegraphics[scale=0.26]{NC_sens_ss0.eps}
			\end{figure}
	\end{columns}
\end{frame}



\begin{frame}
	\frametitle{Results - Statistical Uncertainties}
	{\small Varying the Protons on Target (POT) for the MicroBooNE detector gives:}
	\begin{columns}
		\column{0.5\textwidth}
			\begin{figure}[h]
				\includegraphics[scale=0.26]{POTWgtFar_ms90.eps}
			\end{figure}

		\column{0.5\textwidth}
			\begin{figure}[h]
				\includegraphics[scale=0.25]{ms5s_sinvspot_overlay_far.eps}
			\end{figure}	

	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Results - Statistical Uncertainties}
	{\small Similarly, varying the reconstruction efficiency for each detector individually and both together gives:}
	\begin{columns}
		\column{0.28\textwidth}
			\begin{figure}[h]
				\includegraphics[scale=0.18]{ms5s_sinvsRE_overlay_near.eps}
			\end{figure}
		{\small (a)LAr1ND}

		\column{0.28\textwidth}
			\begin{figure}[h]
				\includegraphics[scale=0.18]{ms5s_sinvsRE_overlay_far.eps}
			\end{figure}
			{\small (b)MicroBooNE}
	
		\column{0.28\textwidth}
			\begin{figure}[h]
				\includegraphics[scale=0.18]{ms5s_sinvsRE_overlay_BOTH.eps}
			\end{figure}
			{\footnotesize (c)LAr1ND+MicroBooNE}
	\end{columns}
	\setlength{\tabcolsep}{2mm}
\end{frame}

\begin{frame}
	\frametitle{Results - Systematic Uncertainties}
	{\small Systematic uncertainty is set to 20\% by default and varied between 10\% and 100\% for both detectors}
	\begin{columns}
		\column{0.5\textwidth}
			\begin{figure}[h]
				\includegraphics[scale=0.25]{Fracsys_ms90.eps}
			%{\small (a)90\% CL}
			\end{figure}

		%\column{0.3\textwidth}
		%	\begin{figure}[h]
		%		\includegraphics[scale=0.25]{Fracsys_ms3s.eps}
		%	%{\small (b)3$\sigma$ CL}
		%	\end{figure}
	
		\column{0.5\textwidth}
			\begin{figure}[h]
				\includegraphics[scale=0.25]{Fracsys_ms5s.eps}
			%{\small (c)5$\sigma$ CL}
			\end{figure}
	\end{columns}

\end{frame}

\begin{frame}
	\frametitle{Results - Systematic Uncertainties}
	{\small As mentioned before, the cross-section has been energy independent. If we consider energy dependence for NC interactions $E\approx1GeV$, cross-section scales with $E^{2}$.\\}
	\begin{figure}[h]
		\includegraphics[scale=0.32]{EnergyDependence_ms90.eps}
	\end{figure} 
\end{frame}

\begin{frame}
	\frametitle{Conclusion}
	\begin{itemize}
		\item To increase sensitivity, increasing reconstruction efficiency by about 10\% in MicroBooNE (60\%) would correspond to roughly the same increase for 10\% increase in POT, concluding we are limited by far detector statistics.
		\item This increase could compensate the loss in sensitivity seen near $\Delta \approx 1 eV^{2}$ when including the energy dependence of the cross section.
		\item Further studies include formulating a more comprehensive correlation matrix.
	\end{itemize}
\end{frame}





%
%\begin{frame}
%	\frametitle{Appendix1}
%	3D Plot of ss Signal
%			\begin{figure}[h]
%				%\includegraphics[scale=0.5]{3dss.eps}
%			\end{figure}
%\end{frame}
%
%\begin{frame}
%	\frametitle{Appendix2}
%	POT Results for LAr1ND
%\begin{columns}
%		\column{0.5\textwidth}
%			\begin{figure}[h]
%				\includegraphics[scale=0.2]{POTWgtNear_ms90.eps}
%			\end{figure}
%
%		\column{0.5\textwidth}
%			\begin{figure}[h]
%				%\includegraphics{ms5s_sinvspot_overlay_near.eps}
%			\end{figure}	
%
%	\end{columns}
%\end{frame}
%
%
\end{document}